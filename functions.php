<?php
/**
* Enqueue scripts and styles.
*/
function duke(){
	$version_constant = time();
	wp_enqueue_script('modernizr-js', get_stylesheet_directory_uri() . '/js/modernizr-2.8.3.min.js', array(
		'jquery'
	) , $version_constant, false);

	wp_enqueue_style( 'custom-font', 'https://use.typekit.net/dsi1ubp.css', array(), $version_constant, false );
	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', array(
		'jquery'
	) , $version_constant, true);

}

add_action('wp_enqueue_scripts', 'duke');

//svg support
function cc_mime_types($mimes) {
	$mimes['svg'] = 'image/svg+xml';
	return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

//gravity form
add_filter( 'gform_validation_message_1', 'change_message', 10, 2 );
function change_message( $message, $form ) {
    return "<div class='validation_error'>Oops! Looks like there was a problem. Please check your form and fill out everything required.</div>";
}

// Gravity Forms anchor - disable auto scrolling of forms
add_filter( 'gform_confirmation_anchor', '__return_false' );
