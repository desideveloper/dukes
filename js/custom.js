(function($) {
  //diamond number list modifications
  $('.number-list li').each(function(){
    var index=$(this).index();
    $(this).prepend("<span class='hexagon'><span class='number'>"+(index + 1)+"</span></span>");
  });
  //diamond img adjustments
  //$('.hexa-img-wrapper .et_pb_testimonial_portrait').append('<div class="hexTop"></div><div class="hexBottom"></div>');
  //cloning of menu nav link for phone number for mobile
  $('body #top-menu li.phone-number a').clone().appendTo('.logo_container').addClass('mobile-nav-number');
  //document ready items
  $(document).ready(function(){
    //addition of custom file upload msgs
    $('#contact-us .gform_wrapper li.gfield.file-upload .ginput_container_fileupload').append("<span class='customMsg'>( .PDF, .DOC and .DOCX files only )</span><span class='secondaryMsg'>( .PDF, .DOC and .DOCX files only )</span>");
    $('#contact-us .gform_wrapper li.gfield.file-upload .ginput_container_fileupload .screen-reader-text').html("( .PDF, .DOC and .DOCX files only )");
    //file upload change function
    $(document).on('change','#contact-us .gform_wrapper li.gfield.file-upload input[type="file"]',function() {
      var extension=$(this).val().replace(/^.*\./, '');
      var fileName = $(this).val();
      if(fileName.length >0){
        var file=$('#contact-us .gform_wrapper li.gfield.file-upload input')[0].files[0].name;
        $(this).parents('li.gfield.file-upload').find('.customMsg').html("( "+file+" )");
        $('#contact-us .gform_wrapper li.gfield .ginput_preview strong').html(file);
        $('#contact-us .gform_wrapper li.gfield.file-upload .ginput_container_fileupload .screen-reader-text').html("( "+file+" )");
        $(this).parents('li.gfield.file-upload').addClass('populated');
      }
      else{
        $(this).parents('li.gfield.file-upload').find('.customMsg').html("( .PDF, .DOC and .DOCX files only )");
        $('#contact-us .gform_wrapper li.gfield .ginput_preview strong').html(" .PDF, .DOC and .DOCX files only ");
        $('#contact-us .gform_wrapper li.gfield.file-upload .ginput_container_fileupload .screen-reader-text').html("( .PDF, .DOC and .DOCX files only )");
        $(this).parents('li.gfield.file-upload').removeClass('populated');
      }
      if(extension== "pdf" || extension == "PDF"  || extension == "doc" || extension == "DOC" || extension == "docx" || extension == "DOCX" ){
        $('#contact-us .gform_wrapper li.gfield.file-upload').removeClass('invalid');
      }else{
        if(fileName.length >0){
          $('#contact-us .gform_wrapper li.gfield.file-upload').addClass('invalid');
          if($('#contact-us .gform_validation_error .file-upload .ginput_container_fileupload + .validation_message').length){
            $('#contact-us .gform_wrapper li.file-upload .validation_message').html("( .PDF, .DOC and .DOCX files only )");

          }else{
            $('<div class="gfield_description validation_message">( .PDF, .DOC and .DOCX files only )</div>').insertAfter('#contact-us .gform_validation_error li.file-upload .ginput_container_fileupload');
            $('#contact-us .gform_wrapper li.file-upload').addClass('gfield_error');
          }

        }
        else{
          $('#contact-us .gform_wrapper li.gfield.file-upload').removeClass('invalid');
        }
      }
    });
    //email varification function
    function validateEmail(isEmail) {
      var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
      if (filter.test(isEmail)) {
        return true;
      }
      else {
        return false;
      }
    }
    //email validation on blur
    $('#contact-us .valid-email input[type="text"]').on('blur', function(){
      var isEmail = $(this).val();
      if (validateEmail(isEmail)) {
        $('#contact-us .valid-email').removeClass('invalid');
      }else{
        $('#contact-us .valid-email').addClass('invalid');
      }
    });
    // text fields validation on blur
    $('#contact-us .valid-text input[type="text"]').on('blur', function(){
      if( !$(this).val() ) {
        $(this).closest('.valid-text').addClass('invalid');
      }else{
        $(this).closest('.valid-text').removeClass('invalid');
      }
    });

    //scroll sections function on href based anchors
    $('a[href*=#]:not([href=#])').click(function(x) {
      x.stopPropagation();
      if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'')
      || location.hostname == this.hostname) {

        var target = $(this.hash);
        target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
        if (target.length) {
          setTimeout(function(){
            $('html,body').animate({
              scrollTop: target.offset().top-$('#main-header').height()
            }, 500);
          },500);

          return false;
        }
      }
    });
    //arrow down button on hero sections
    $('.hero-sec .et_pb_fullwidth_header_scroll a').click(function(x){
      x.stopPropagation();
      x.preventDefault();
      $(this).die().undelegate().stop()
      // $('html, body').die().undelegate().stop().off('scrollTop');
      var headerHt  = $('#main-header').outerHeight();
      var secOffset = $(this).parents('.et_pb_section').next('.et_pb_section').offset().top;
      $('html, body').animate({ scrollTop: secOffset - headerHt }, '500');
    })
    //tabs converstion in accordion on mobile
    $('.et_pb_tabs_controls li').each(function(){
      var number=$(this).index();
      var txt=$(this).children('a').text();
      $(this).parent().next('.et_pb_all_tabs').children('.et_pb_tab').eq( number ).prepend("<h2 class='mobile-trigger'>"+txt+"</h2>");
    });
    $('.et_pb_all_tabs .et_pb_tab:first-child .mobile-trigger').addClass('active');
    //accordion functionality for tabs
    $(document).on('click', '.mobile-trigger' , function(){
      var $this = $(this),
      headerHt  = $('#main-header').outerHeight();
      $this.addClass('active');
      $this.next('.et_pb_tab_content').slideDown();
      $this.parent().siblings('.et_pb_tab').children('.mobile-trigger').removeClass('active');
      $this.parent().siblings('.et_pb_tab').children('.et_pb_tab_content').slideUp();

      setTimeout(function(){
        var secOffset = $this.offset().top;
        //console.log(secOffset);
        if( $(window).width() < 768){
          setTimeout(function(){
            $('html, body').animate({ scrollTop: secOffset - headerHt }, '500');
          },300);
        }else{
          setTimeout(function(){
            $('html').animate({ scrollTop: secOffset - headerHt - 36 }, '500');
          },300);
        }
      },500);

    });

  })
  $(window).load(function(){
    var full_url = document.URL;
    var url_array = full_url.split('/')
    var last_segment = url_array[url_array.length-1];
    //console.log( last_segment );
    if(last_segment.length){
      var headerHt  = $('#main-header').height();
      var secOffset = $(last_segment).offset().top;
      $('html, body').animate({ scrollTop: secOffset - headerHt }, '500');
    }
    $('#mobile_menu li a').on('click' , function(x){
      var full_url = $(this).attr('href');
      var url_array = full_url.split('/')
      var last_segment = url_array[url_array.length-1];
      //console.log( last_segment );
      if($('body.home').length && last_segment == "#contact-us"){
        $('html, body').die().undelegate().stop().off('scrollTop');
        x.preventDefault();
        x.stopPropagation();
        var headerHt  = $('#main-header').height();
        var secOffset = $(last_segment).offset().top;
        $('html, body').animate({ scrollTop: secOffset - headerHt }, '0');
      }
    })
  })

  jQuery(document).bind('gform_confirmation_loaded', function(event, formId){
    $('html, body').animate({
      scrollTop: $("#gform_confirmation_wrapper_1").offset().top - 250
    }, 400);

  });
})(jQuery);
